// de QT
#include "caidalibre.h"
#include <QApplication>

// Biblioteca estandar
#include <iostream>
#include <cstring>

//------------------------------------------------------------------------------------
// Declaracion de Variables Globales
//------------------------------------------------------------------------------------


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    caidalibre ventana_principal;

    ventana_principal.show();  // Muestra la ventana

    return a.exec();           // Retorna a QT para que maneje los eventos
}
