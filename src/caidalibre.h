#ifndef CAIDALIBRE_H
#define CAIDALIBRE_H

#include <QMainWindow>

//----------------------------------------------------------------------------------
// Definicion de Estructuras y Tipos
//----------------------------------------------------------------------------------

typedef struct {
    double tiempo;
    double altura;
    double velocidad_final;
    double gravedad;
} variables;

enum estado { INGRESADO, CALCULADO };

enum tipo_dato { TIEMPO, VELOCIDAD, DISTANCIA };

enum unidad {  };

/////////////////////////////////////////////////////////////////////////////////////


namespace Ui {
class caidalibre;
}

class caidalibre : public QMainWindow
{
    Q_OBJECT

public:
    explicit caidalibre(QWidget *parent = 0);
    ~caidalibre();

    double altura(double t, double vf);
    double velocidadFinal(double g, double t);
    double funcionPotencia(double base, double exponente);
    double velocidadFinal2(double g, double h);
    double altura2(double g, double t);
    double tiempoCaida(double g , double h);

    void grafico_1 (int x1, int y1, int forma, QString area , QString titulo);
    void grafico_2(int x1 , int y1, int fun, QString var, QString titulo);

private slots:

    void on_input_altura_valueChanged(double arg1);

    void on_input_velocidad_valueChanged(double arg1);

    void on_input_tiempo_valueChanged(double arg1);

    void on_input_gravedad_valueChanged(double arg1);

    void on_pushButton_clicked();

private:
    Ui::caidalibre *ui;

    variables datos = { 0, 0, 0, 0 },
              resultados = { 0, 0, 0, 0 };

    //------------------------------------------------------------------------------------
    // Declaracion de Funciones propias
    //------------------------------------------------------------------------------------
    void calcular ();
};

#endif // CAIDALIBRE_H
