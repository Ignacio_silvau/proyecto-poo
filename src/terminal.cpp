/*
*   Interfaz de usuario en la terminal
*/

#ifdef TERMINAL

// de QT
#include "caidalibre.h"
#include <QApplication>

#include <QtCharts>


// Bibliotecas estandar
#include <iostream>
#include <cstring>
#include <math.h>

// Definicion de los "literals" de los menus

//Menú de gráficos
#define MENU_TVF "\nMenú de Graficos Disponibles:\n\na) Grafico Velocidad final vs Tiempo\nb) Gráfico de Tiempo\nc) Gráfico de Velocidad Final\nd) Mostrar todos\n\nPara cerrar el programa ingrese 'p'\n"
#define MENU_AVF "\nMenú de Graficos Disponibles:\n\na) Gráfico de Altura\nb) Gráfico de Velocidad Final\nc) Mostrar todos\n\nPara cerrar el programa ingrese 'p'\n"
#define MENU_TODOS "\nMenú de Graficos Disponibles:\n\na) Grafico Velocidad final vs Tiempo\nb) Gráfico de Tiempo\nc) Gráfico de Velocidad Final\nd) Gráfico de Altura\ne) Mostrar todos\n\nPara cerrar el programa ingrese 'p'\n"

#define MENU_VELOCIDAD "\nSeleccione la unidad de medida que posee: \na) Metros/segundos [m/s]\nb) Kilometro/hora [Km/h]"
#define MENU_TIEMPO "\nSeleccione la unidad de medida que posee: \na) Segundos [s]\nb) Minutos [min]\nc) Horas [h]"
#define MENU_DISTANCIA "\nSeleccione la unidad de medida que posee: \na) Milímetro [mm]\nb) Centímetro [cm]\nc) Metro [m]\nd) Kilometro [Km]"

#define INGRESADO "\x1b[33m(Ingresado)\x1b[39m"
#define INGRESADA "\x1b[33m(Ingresada)\x1b[39m"
#define CALCULADO "\x1b[34m(Calculado)\x1b[39m"
#define CALCULADA "\x1b[34m(Calculada)\x1b[39m"

#define ERROR_ENTRADA 1
#define ERROR_OPCION  2

using namespace QtCharts;
using namespace std;

//----------------------------------------------------------------------------------
// Definicion de Estructuras y Tipos
//----------------------------------------------------------------------------------

struct variables {
    double tiempo;
    double altura;
    double velocidad_final;
    double gravedad;
};

enum tipo_dato { TIEMPO, VELOCIDAD, DISTANCIA };

//------------------------------------------------------------------------------------
// Declaracion de Variables Globales
//------------------------------------------------------------------------------------

QWidget *ventana_ptr = nullptr;

QGridLayout *layout = new QGridLayout;

struct variables datos = { 0, 0, 0, 0 },
                 resultados = { 0, 0, 0, 0 };

int fila = 0;
int columna = 0;

//------------------------------------------------------------------------------------
// Declaracion de Funciones
//------------------------------------------------------------------------------------

void grafico_1 (int x1, int y1, int forma, QString area , QString titulo);
void grafico_2(int x1 , int y1, int fun, QString var, QString titulo);

double altura(double t, double vf) {
    double h;

    h = vf / 2 * t;

    return h;
}

double velocidadFinal(double g, double t) {
        double vf;
        vf = g * t;
        return vf;
}

double funcionPotencia(double base, double exponente) {
    double resultado = 0;

    if (exponente == 0 || exponente <= 0) resultado = 1;
    else resultado = base * funcionPotencia(base, exponente - 1);

    return resultado;
}

double velocidadFinal2(double g, double h) {
    double resultado;

    resultado= sqrt(fabs(2*g*h));
    return resultado;
}

double altura2(double g, double t) {
    double resultado;
    resultado= (g*funcionPotencia(t,2))/2;

    return resultado;
}

double tiempoCaida(double g , double h) {

    double base, resultado;
    base = (2 * h) / g;
    resultado = sqrt(fabs(base));

    return resultado;
}

// Funciones de utileria para terminar el programa
void salir ()
{
    cout << "\n\x1b[32mFin de Programa...\x1b[39m\n\n";
    exit (0);
}
void salir (const char *mensaje)
{
    cout << mensaje << endl;
    exit (0);
}

void mostrar_error (int tipo_error)
{
    if (tipo_error == ERROR_ENTRADA) cout << "\n\x1b[31mERROR: Entrada invalida\x1b[39m\n";
    if (tipo_error == ERROR_OPCION) cout << "\n\x1b[31mERROR: Ingrese una opcion valida\x1b[39m\n\n";
}

// Funcion para mostrar el menu principal
void menu ()
{
    cout << "\nSeleccione el dato que posee: \n"
            "a) Altura\n"
            "b) Velocidad Final\n"
            "c) Tiempo de Caida\n"
            "d) Gravedad\n"

            "\nPara cerrar la interfaz ingrese 'p'\n\n"

            "Ingrese su respuesta: ";
}

// Funcion que muestra el menu para preguntar por otro dato
int menu_extra ()
{
    char respuesta;
    char buffer[128] = { 0 };

    while (1)
    {
        cout<<"\n¿Desea Ingresar otro dato?\n"
              "a) Si\n"
              "b) No\n"
              "\nIngrese su respuesta: ";

        fgets(buffer, 127, stdin);

        if (sscanf (buffer, " %c", &respuesta) == 1)
        {
            if (respuesta == 'a' || respuesta == 'A') return 1;
            if (respuesta == 'b' || respuesta == 'B') return 0;
            else mostrar_error(ERROR_OPCION);
        } else mostrar_error(ERROR_OPCION);
    }
}

// Funcion para parsear la entrada de datos numericos
double input_double (const char *mensaje)
{
    char buffer[128] = { 0 };
    double entrada = 0;

    cout << mensaje << ": ";

    while (1)
    {
        fgets(buffer, 127, stdin);

        if ((sscanf (buffer, "%lf", &entrada) == 1) && entrada > 0)
        {
            return entrada;

        } else {
            mostrar_error(ERROR_ENTRADA);
            cout << mensaje << " otra vez: ";
        }
    }
}

// Funcion para ingresar datos segun el tipo y realizar las conversiones
double input_dato (const char *mensaje, enum tipo_dato dato)
{
    bool seguir = true;

    char buffer[128] = { 0 };
    char respuesta;

    if (dato == TIEMPO) cout << MENU_TIEMPO << endl;
    if (dato == VELOCIDAD) cout << MENU_VELOCIDAD << endl;
    if (dato == DISTANCIA) cout << MENU_DISTANCIA << endl;

    cout << "\nPara cerrar la interfaz ingrese 'p'\n" << endl;

    while (seguir) {

        cout << "Ingrese su respuesta: ";

        fgets (buffer, 127, stdin);
        sscanf (buffer, " %c", &respuesta);

        // Tiempo
        if (dato == TIEMPO)
        {
            if (respuesta == 'a' || respuesta == 'A') return input_double(mensaje);
            if (respuesta == 'b' || respuesta == 'B') return input_double(mensaje) * 60;
            if (respuesta == 'c' || respuesta == 'C') return input_double(mensaje) * 60 * 60;
            if (respuesta == 'p' || respuesta == 'P') salir ();
            else mostrar_error(ERROR_OPCION);
        }

        // Velocidad
        if (dato == VELOCIDAD)
        {
            if (respuesta == 'a' || respuesta == 'A') return input_double(mensaje);
            if (respuesta == 'b' || respuesta == 'B') return input_double(mensaje) / 3.6;
            if (respuesta == 'p' || respuesta == 'P') salir ();
            else mostrar_error(ERROR_OPCION);
        }

        // Distancia
        if (dato == DISTANCIA)
        {
            if (respuesta == 'a' || respuesta == 'A') return input_double(mensaje) / 1000;
            if (respuesta == 'b' || respuesta == 'B') return input_double(mensaje) / 100;
            if (respuesta == 'c' || respuesta == 'C') return input_double(mensaje);
            if (respuesta == 'd' || respuesta == 'D') return input_double(mensaje) * 1000;
            if (respuesta == 'p' || respuesta == 'P') salir ();
            else mostrar_error(ERROR_OPCION);
        }
    }

    return input_double(mensaje);
}

// Funcion que muestra los datos que ha ingresado el usuario
// Muestra los calculos que pueden hacerse con los datos disponibles
// altura (tiempo, velocidad_final)
// velocidadFinal (gravedad , tiempo)
// velocidadFinal2 (gravedad, altura)
// altura2 (gravedad, tiempo)
// tiempoCaida (gravedad , altura)
int mostrar_datos (variables datos)
{
    cout << "\n\n\x1b[32mTus datos son los siguientes: \x1b[39m\n" << endl;

    // Si se ingreso una gravedad se almacena en resultados
    if (datos.gravedad) resultados.gravedad = datos.gravedad;

    // Calcula la altura si el usuario no la ingreso y si se tiene tiempo y velocidad final o gravedad y tiempo
    if (datos.altura == 0 && ((datos.velocidad_final && datos.tiempo >= 0) || (datos.tiempo >= 0 && datos.gravedad >= 0)))
    {
        if (datos.velocidad_final && datos.tiempo) {
            resultados.altura = altura (datos.tiempo, datos.velocidad_final);

        } else {
            resultados.altura = altura2 (datos.gravedad, datos.tiempo);
        }
    } else resultados.altura = datos.altura;

    // Calcula Tiempo de caida si el usuario no la ingreso y si se tiene gravedad y altura
    if (datos.tiempo == 0 && datos.altura > 0 && datos.gravedad > 0) {
        resultados.tiempo = tiempoCaida (datos.gravedad, datos.altura);
    } else resultados.tiempo = datos.tiempo;

    // Calcula la velocidad final dependiento de si el usuario no la ingreso y si se tiene el tiempo de caida y la graveda o la altura y la gravedad
    if (datos.velocidad_final == 0 && ((datos.tiempo > 0 && datos.gravedad >= 0) || (datos.altura && datos.gravedad >= 0)))
    {
        if (datos.tiempo) {
           resultados.velocidad_final = velocidadFinal (datos.gravedad, datos.tiempo);
        } else if (datos.altura) {
           resultados.velocidad_final = velocidadFinal2 (datos.gravedad, datos.altura);
        }
    } else resultados.velocidad_final = datos.velocidad_final;

    // Mostrar los datos ingresados y los calculos de los faltantes
    if (datos.altura == 0 && resultados.altura > 0) {
        cout << "La Altura " CALCULADA " es: " << resultados.altura << "[M]" << endl;
    } else {
        if (datos.altura > 0) cout << "La Altura " INGRESADA " es: " << datos.altura << "[M]" << endl;
        else cout << "\x1b[31mNo hay suficientes datos para calcular la Altura\x1b[39m" << endl;
    }

    if (datos.velocidad_final == 0 && resultados.velocidad_final > 0) {
        cout << "La Velocidad Final " CALCULADA " es: " << resultados.velocidad_final << "[M/S]" << endl;
    } else {
        if (datos.velocidad_final > 0) cout << "La Velocidad Final " INGRESADA " es: " << datos.velocidad_final << "[M/S]" << endl;
        else cout << "\x1b[31mNo hay suficientes datos para calcular La Velocidad Final\x1b[39m" << endl;
    }

    if (datos.tiempo == 0 && resultados.tiempo > 0) {
        cout << "El Tiempo de Caida " CALCULADO " es: " << resultados.tiempo << "[S]" << endl;
    } else {
        if (datos.tiempo > 0) cout << "El Tiempo de Caida " INGRESADO " es: " << datos.tiempo << "[S]" << endl;
        else cout << "\x1b[31mNo hay suficientes datos para calcular El Tiempo de Caida\x1b[39m" << endl;
    }

    if (datos.gravedad == 0 && resultados.gravedad > 0) {
        cout << "La Gravedad es " CALCULADA ": " << datos.gravedad << "[M/S²]" <<endl;
    } else {
        if (datos.gravedad) cout << "La Gravedad " INGRESADA " es: " << datos.gravedad << "[M/S²]" <<endl;
        else cout << "\x1b[31mNo hay suficientes datos para calcular la Gravedad\x1b[39m" << endl;
    }

    if (resultados.altura > 0 && resultados.gravedad > 0 && resultados.tiempo > 0 && resultados.velocidad_final > 0)
        return 1;
    else return 0;
}

void grafico_1 (int x1, int y1, int forma, QString area , QString titulo) // forma = 0 para grafico triangulares y forma = y1 para graficos cuadrilateros
{
    QLineSeries *linea = new QLineSeries();

       linea->append(0,forma);
       linea->append(x1,y1);

       QLineSeries *linea2 = new QLineSeries();

       linea2->append(0,0);
       linea2->append(x1,0);

       QAreaSeries *Area = new QAreaSeries(linea,linea2);
       Area->setName(area);
       QChart *grafico = new QChart();

       grafico->addSeries(Area);
       grafico->createDefaultAxes();
       grafico->axes(Qt::Vertical).first()->setRange(0,y1+5);
       grafico->axes(Qt::Horizontal).first()->setRange(0,x1+5);
       grafico->setTitle(titulo);

       QChartView *ventanalinea = new QChartView(grafico);
       ventanalinea->setRenderHint(QPainter::Antialiasing);

       layout->addWidget(ventanalinea, fila, columna);
}

void grafico_2(int x1 , int y1, int fun, QString var, QString titulo){
    QLineSeries *exp = new QLineSeries();
    if(fun==0){
        for (int i=1 ; i<x1+1 ; i++){
        exp->append(i,altura2(y1,i));
    }
    }else if (fun==1){
        for (int i=1 ; i<x1+1 ; i++){
        exp->append(i,velocidadFinal2(y1,i));
        }
    }else{
        for (int i=1 ; i<x1+1 ; i++){
        exp->append(i,tiempoCaida(y1,i));
     }
    }

    exp->setName(var);
    QChart *chart = new QChart();
    chart->addSeries(exp);
    chart->createDefaultAxes();
    //chart->axes(Qt::Vertical).first()->setRange(0,50);
    //chart->axes(Qt::Horizontal).first()->setRange(1,50);
    chart->setTitle(titulo);
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    layout->addWidget(chartView); // Agrega el Widget del grafico al layout de la ventana
}

int mostrar_graficos()
{
    bool seguir = false;
    char buffer[128] = { 0 };         // buffer para almacenar la entrada del usuario, inicializado a 0
    char respuesta;

    if (resultados.tiempo != 0 && resultados.altura != 0 && resultados.velocidad_final != 0)
    {
        cout << MENU_TODOS << endl;
        seguir = true;
    } else if(resultados.altura != 0 && resultados.velocidad_final != 0) {
        cout << MENU_AVF << endl;
        seguir = true;
    } else if (resultados.tiempo != 0 && resultados.velocidad_final != 0) {
        cout << MENU_TVF << endl;
        seguir = true;
    } else cout << "\n\x1b[31mNo hay suficientes datos para Crear un Grafico\x1b[39m" << endl;

    while (seguir)
    {
        cout<<"Ingrese su respuesta: ";

        fgets (buffer, 127, stdin);
        sscanf (buffer, " %c", &respuesta);

        if (resultados.tiempo != 0 && resultados.altura != 0 && resultados.velocidad_final != 0) {
            if (respuesta == 'a' || respuesta == 'A') {
                grafico_1 (resultados.tiempo, resultados.velocidad_final,0, "altura", "Grafico Velocidad final vs Tiempo");
                return 1;
            }

            if (respuesta == 'b' || respuesta == 'B') {
                grafico_2 (resultados.altura, resultados.gravedad, 3, "Tiempo Caida", "Tiempo de caida en funcion de la altura");
                return 1;
            }

            if (respuesta == 'c' || respuesta == 'C') {
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final", "Grafico velocidad en funcion de la altura");
                return 1;
            }

            if (respuesta == 'd' || respuesta == 'D') {
                grafico_2(resultados.tiempo, resultados.gravedad, 3, "Altura", "Altura en funcion del tiempo");
                return 1;
            }

            if (respuesta == 'e' || respuesta == 'E') {
                grafico_1 (resultados.tiempo, resultados.velocidad_final,0, "altura", "Grafico Velocidad final vs Tiempo");
                grafico_2 (resultados.altura, resultados.gravedad, 3, "Tiempo Caida", "Tiempo de caida en funcion de la altura");
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final", "Grafico velocidad en funcion de la altura");
                grafico_2(resultados.tiempo, resultados.gravedad, 3, "Altura", "Altura en funcion del tiempo");
                return 1;
            }

            if (respuesta == 'p' || respuesta == 'P') salir ();
        }

        if(resultados.tiempo != 0 && resultados.velocidad_final != 0)
        {
            // Si se elige el grafico de Velocidad final vs Tiempo
            if (respuesta == 'a' || respuesta == 'A') {
                grafico_1 (resultados.tiempo, resultados.velocidad_final,0, "altura", "Grafico Velocidad final vs Tiempo");
                return 1;
            }
            // Si se elige el grafico de tiempo
            if (respuesta == 'b' || respuesta == 'B') {
                grafico_2(resultados.altura, resultados.gravedad, 3, "Tiempo Caida", "Tiempo de caida en funcion de la altura");
                return 1;
            }
            // Si se elige el grafico de velocidad final
            if (respuesta == 'c' || respuesta == 'C') {
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final", "Grafico velocidad en funcion de la altura");
                return 1;
            }

            if (respuesta == 'd' || respuesta == 'D') {
                grafico_1 (resultados.tiempo, resultados.velocidad_final, 0, "Altura", "Grafico Velocidad final vs Tiempo");
                grafico_2(resultados.altura, resultados.gravedad, 3, "Tiempo Caida", "Tiempo de caida en funcion de la altura");
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final", "Grafico velocidad en funcion de la altura");
                return 1;
            }

            // Si se elige terminar el programa
            if (respuesta == 'p' || respuesta == 'P') salir();
        }

        // Si se tiene la altura y la velocidad final se pueden mostrar los graficos de altura y velocidad final
        if ( resultados.altura != 0 && resultados.velocidad_final != 0)
        {
            // Si se elige el grafico de altura
            if ( respuesta == 'a' || respuesta == 'A' )
            {
                grafico_2(resultados.tiempo, resultados.gravedad, 3, "Altura", "Altura en funcion del tiempo");
                return 1;
            }

            // Si se elige el grafico de velocidad final
            if (respuesta == 'b' || respuesta == 'B') {
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final","Grafico velocidad en funcion del tiempo");
                return 1;
            }

            if (respuesta == 'c' || respuesta == 'C') {
                grafico_2(resultados.tiempo, resultados.gravedad, 3, "Altura", "Altura en funcion del tiempo");
                grafico_2(resultados.altura, resultados.gravedad, 1, "Velocidad final","Grafico velocidad en funcion del tiempo");
                return 1;
            }

            if (respuesta == 'p' || respuesta == 'P') salir();
        }

        mostrar_error(ERROR_OPCION);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    caidalibre ventana_principal;

    QWidget *ventana = new QWidget;  // Crea una subventana

    ventana_ptr = ventana;           // Inicializa el puntero global a la subventana

    int mostrar = 0;

    char buffer[128] = { 0 };         // buffer para almacenar la entrada del usuario, inicializado a 0
    char respuesta;

    bool seguir = true;

    cout << "\n\nBienvenido a la Interfaz Gráfica de Caída Libre\n\n"
    "\nEste programa le permite calcular problemas de caída libre.\nTales como:\n*tiempo de caida(ingresar gravedad y altura)\n*Altura(Ingresar tiempo y velocidad final ó gravedad y tiempo)\n*velocidadFinal(Ingresar gravedad y altura ó gravedad y tiempo) \n"
    "Tenga en cuenta que si no se ingresa la gravedad, se asigna automaticamente un valor igual a 9.8 [M/S²]\n";

    //--------------------------------------------------------------------------------------
    // Ciclo principal del programa

    while (seguir)
    {
        menu ();

        fgets (buffer, 127, stdin);
        sscanf (buffer, " %c", &respuesta);

        switch (respuesta)
        {
            case 'a':
            case 'A':
            {
                if ((datos.altura = input_dato("\nIngrese la altura", DISTANCIA)))
                {
                    if (!mostrar_datos (datos))
                        if (menu_extra()) continue;
                        else seguir = false;
                    else {
                        cout << "\n\n\x1b[32mYa estan todos los datos...\x1b[39m\n\n";
                        seguir = false;
                    }
                 }

            } break;

            case 'b':
            case 'B':
            {
                if ((datos.velocidad_final = input_dato ("\nIngrese velocidad final", VELOCIDAD)))
                {
                    if (!mostrar_datos (datos))
                        if (menu_extra()) continue;
                        else seguir = false;
                    else {
                        cout << "\n\n\x1b[32mYa estan todos los datos...\x1b[39m\n\n";
                        seguir = false;
                    }
                }

            } break;

            case 'c':
            case 'C':
            {
                if ((datos.tiempo = input_dato ("\nIngrese el tiempo", TIEMPO)))
                {
                    if (!mostrar_datos (datos))
                        if (menu_extra()) continue;
                        else seguir = false;
                    else {
                        cout << "\n\n\x1b[32mYa estan todos los datos...\x1b[39m\n\n";
                        seguir = false;
                    }
                }

            } break;

            case 'd':
            case 'D':
            {
                if ((datos.gravedad = input_double ("\nIngrese la gravedad")))
                {
                    if (!mostrar_datos (datos))
                        if (menu_extra()) continue;
                        else seguir = false;
                    else {
                        cout << "\n\n\x1b[32mYa estan todos los datos...\x1b[39m\n\n";
                        seguir = false;
                    }
                }

            } break;

            case 'p':
            case 'P':
            {
                salir();
            } break;

            default:
                mostrar_error(ERROR_OPCION);
                break;
        }
    }

    mostrar = mostrar_graficos();

    // Mostrar el o los graficos.
    if (mostrar) {

        cout << "\n\n\x1b[32mMostrando grafico/s...Adios\x1b[39m\n" << endl;

        //layout->setVerticalSpacing(0);

        ventana->setLayout(layout);

        ventana_principal.setCentralWidget(ventana);
        ventana_principal.show();

        return a.exec();
    } else salir();
}

#endif
