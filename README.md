# Proyecto Simulación Objeto en Caída Libre

Programa capaz de resolver problemas relacionados al fenomeno fisico de la caida
libre , utilizando adecuadamente la altura , peso del objeto , gravedad y/o punto de 
inicio del movimiento , todas variables calculadas  por el programa 
o ingresadas por el usuario.

## Requisitos para compilar

1. Tener un dispositivo con alguna distribución de  linux o windows 
2. Tener instalado un compilador de C++ actualizado
5. Tener QTcreator en la version mas reciente
6. Tener la libreria de QTcharts para poder compilar la ventana interactiva y las salidas graficas del programa

## Instrucciones para compilar

### En Linux

1. Clonar el repositorio con `https://gitlab.com/Ignacio_silvau/proyecto-poo.git`
2. Ir a la carpeta donde esta el archivo del proyecto `.pro` en `proyecto-poo/src`
3. Abrirlo con `QT Creator`
4. Compilar y Ejecutar el programa con el boton `Run` o `Ctrl + R`


## Integrantes

- Tomás Campusano Gaete    201930518-2
- Ignacio Silva Uribe
        201930560-3
- Christian Riquelme Parra
        201930520-4
- Vicente Carreño Escobar
        201930516-6

## Informacion del curso

Profesor:  F. Martínez
Ramo: Programación orientado a objetos  
Paralelo: 200

